﻿#include <iostream>

// MY DEBUG определена
//#define MY_DEBUG

template<typename T, int N, int M>
struct MassWrapper
{
    T mass[N][M];
};

template<typename T, int N, int M>
class Matrix
{
public:
    // конструктор
    Matrix()
    {
#ifdef MY_DEBUG
        std::cout << "Constructor" << std::endl;
#endif
        m_n = N;
        m_m = M;
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                m_mat[i][j] = 0;
            }
        }
    }

    Matrix(const T mass[N][M])
    {
#ifdef MY_DEBUG
        std::cout << "Constructor" << std::endl;
#endif
        m_n = N;
        m_m = M;
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                m_mat[i][j] = mass[i][j];
            }
        }
    }

    Matrix(const MassWrapper<T, N, M>& mass)
    {
#ifdef MY_DEBUG
        std::cout << "Constructor" << std::endl;
#endif
        m_n = N;
        m_m = M;
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < M; j++)
            {
                m_mat[i][j] = mass.mass[i][j];
            }
        }
    }
    
    Matrix(const Matrix<T, N, M>& mat)
    {
#ifdef MY_DEBUG
        std::cout << "Copy constructor" << std::endl;
#endif
        m_n = mat.m_n;
        m_m = mat.m_m;

        for (int i = 0; i < m_n; i++)
        {
            for (int j = 0; j < m_m; j++)
            {
                m_mat[i][j] = mat.m_mat[i][j];
            }
        }
    }

    int getN() const
    {
        return m_n;
    }
    int getM() const
    {
        return m_m;
    }
    int get(int i, int j) const
    {
        return m_mat[i][j];
    }
    void set(int i, int j, T data)
    {
        m_mat[i][j] = data;
    }
    // Присваивание
    template<typename T, int N, int M>
    Matrix<T, N, M>& operator=(const Matrix<T, N, M>& mat)
    {
#ifdef MY_DEBUG
        std::cout << "Operator = " << std::endl;
#endif
        m_n = mat.getN();
        m_m = mat.getM();
        for (int i = 0; i < m_n; i++)
        {
            for (int j = 0; j < m_m; j++)
            {
                m_mat[i][j] = mat.get(i, j);
            }
        }
        return *this;
    }

    // Оператор сложения
    template<typename T, int N, int M>
    Matrix<T, N, M> operator+(const Matrix<T, N, M>& mat)
    {
#ifdef MY_DEBUG
        std::cout << "Operator+" << std::endl;
#endif
        Matrix<T, N, M> tmp(mat.m_n, mat.m_m);
        for (int i = 0; i < m_n; i++)
        {
            for (int j = 0; j < m_m; j++)
            {
                tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
            }
        }
        return tmp;
    }

    // оператор вычитания
    template<typename T, int N, int M>
    Matrix<T, N, M> operator-(const Matrix<T, N, M>& mat)
    {
#ifdef MY_DEBUG
        std::cout << "Operator-" << std::endl;
#endif
        Matrix<T, N, M> tmp(mat.m_n, mat.m_m);
        for (int i = 0; i < m_n; i++)
        {
            for (int j = 0; j < m_m; j++)
            {
                tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
            }
        }
        return tmp;
    }
    
    // оператор умножения 
    template<typename T, int N, int M>
    Matrix<T, N, M> operator*(const Matrix<T, N, M>& mat)
    {
#ifdef MY_DEBUG
        std::cout << "Operator*" << std::endl;
#endif
        Matrix<T, N, M> tmp;
        for (int i = 0; i < m_n; i++)
        {
            for (int j = 0; j < mat.getM(); j++)
            {
                int sum = 0;
                for (int k = 0; k < m_m; k++)
                {
                    sum += m_mat[i][k] * mat.get(i, j);
                }
                tmp.set(i, j, sum);
            }
        }

        return tmp;
    }

    // поиск определителя
    int DET()
    {
        int det;
        if (m_n == m_m and m_m == 2)
        {
            det = (m_mat[0][0] * m_mat[1][1]) - (m_mat[0][1] * m_mat[1][0]);
        }
        else if (m_n == m_m and m_m == 3)
        {
            det = m_mat[0][0] * m_mat[1][1] * m_mat[2][2] - m_mat[0][0] * m_mat[1][2] * m_mat[2][1]
                - m_mat[0][1] * m_mat[1][0] * m_mat[2][2] + m_mat[0][1] * m_mat[1][2] * m_mat[2][0]
                + m_mat[0][2] * m_mat[1][0] * m_mat[2][1] - m_mat[0][2] * m_mat[1][1] * m_mat[2][0];
        }
        else
        {
            std::cout << "Операция не предусмотрена" << std::endl;
        }
        return det;
    }
    // транспонированная матрица
    Matrix TRANS()
    {
        std::swap(m_n, m_m);
        Matrix tmp(m_n, m_m);
        for (int i = 0; i < m_n; i++)
            for (int j = 0; j < m_m; j++)
                tmp.m_mat[i][j] = m_mat[j][i];
        for (int i = 0; i < m_m; i++)
            delete[] m_mat[i];
        delete m_mat;
        massive(tmp);

        return tmp;
    }
    void massive(const Matrix& mat)
    {
        m_mat = new int* [m_n];
        for (int i = 0; i < m_n; i++)
            m_mat[i] = new int[m_m];
        for (int i = 0; i < m_n; i++)
            for (int j = 0; j < m_m; j++)
                m_mat[i][j] = mat.m_mat[i][j];
    }
    // обратная матрица
    template<typename T, int N, int M>
    Matrix INVERSE(Matrix& mat)
    {
        int det = mat.DET();
        double invdet = 1.0 / det;
        if (det == 0 or m_m != m_n)
        {
            std::cout << "Обратной матрицы не существует" << std::endl;
        }
        else
        {
            double** tmp = new double* [mat.m_m];
            for (int i = 0; i < m_m; i++)
            {
                tmp[i] = new double[m_m];
            }
            if (m_m == 2)
            {
                tmp[0][0] = invdet * mat.m_mat[1][1];
                tmp[0][1] = invdet * -(mat.m_mat[0][1]);
                tmp[1][0] = invdet * -(mat.m_mat[1][0]);
                tmp[1][1] = invdet * mat.m_mat[0][0];
            }
            if (m_m == 3)
            {
                tmp[0][0] = invdet * (mat.m_mat[1][1] * mat.m_mat[2][2] - mat.m_mat[2][1] * mat.m_mat[1][2]);
                tmp[0][1] = invdet * (mat.m_mat[0][2] * mat.m_mat[2][1] - mat.m_mat[0][1] * mat.m_mat[2][2]);
                tmp[0][2] = invdet * (mat.m_mat[0][1] * mat.m_mat[1][2] - mat.m_mat[0][2] * mat.m_mat[1][1]);
                tmp[1][0] = invdet * (mat.m_mat[1][2] * mat.m_mat[2][0] - mat.m_mat[1][0] * mat.m_mat[2][2]);
                tmp[1][1] = invdet * (mat.m_mat[0][0] * mat.m_mat[2][2] - mat.m_mat[0][2] * mat.m_mat[2][0]);
                tmp[1][2] = invdet * (mat.m_mat[1][0] * mat.m_mat[0][2] - mat.m_mat[0][0] * mat.m_mat[1][2]);
                tmp[2][0] = invdet * (mat.m_mat[1][0] * mat.m_mat[2][1] - mat.m_mat[2][0] * mat.m_mat[1][1]);
                tmp[2][1] = invdet * (mat.m_mat[2][0] * mat.m_mat[0][1] - mat.m_mat[0][0] * mat.m_mat[2][1]);
                tmp[2][2] = invdet * (mat.m_mat[0][0] * mat.m_mat[1][1] - mat.m_mat[1][0] * mat.m_mat[0][1]);
            }
            for (int i = 0; i < m_m; i++)
            {
                for (int j = 0; j < m_m; j++)
                {
                    std::cout << tmp[i][j] << std::endl;
                }
            }
            for (int i = 0; i < m_m; i++)
            {
                delete[]tmp[i];
            }
            delete[]tmp;
        }
    }

    ~Matrix()
    {
#ifdef MY_DEBUG
        std::cout << "Destructor" << std::endl;
#endif
    }
    template<typename T, int N, int M>
    friend std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat);
    template<typename T, int N, int M>
    friend std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat);
private:
    int m_n, m_m;
    T m_mat[N][M];
};
template<typename T, int N, int M>
std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat)
{
    for (int i = 0; i < mat.m_n; i++)
    {
        for (int j = 0; j < mat.m_m; j++)
        {
            in >> mat.m_mat[i][j];
        }
    }
    return in;
}
template<typename T, int N, int M>
std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat)
{
    out << "Matrix " << mat.m_n << "x" << mat.m_m << std::endl;
    for (int i = 0; i < mat.m_n; i++)
    {
        for (int j = 0; j < mat.m_m; j++)
        {
            out << mat.m_mat[i][j] << " ";
        }
        out << std::endl;
    }
    return out;
}

using Vec2i = Matrix<int, 2, 1>;
using Vec2d = Matrix<double, 2, 1>;
using Mat22i = Matrix<int, 2, 2>;
using Mat22d = Matrix<double, 2, 2>;

int main()
{
    setlocale(LC_ALL, "Russian");

    Mat22d A({ {

        {1,2},
        {3,4}

        } });
    

    Vec2d X({ {
        {1},
        {1}
        } });

    auto B = A*X;
    std::cout << B;


    return 0;
}